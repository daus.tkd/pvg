from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from time import sleep
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from random import random
#from faker import Faker
import unittest, time, re, datetime
import os
#from dotenv import load_dotenv


class Scenariotest(unittest.TestCase):
    def setUp(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        s=Service(r'C:\Users\Admin\Documents\PVG\chromedriver.exe')
        self.driver = webdriver.Chrome(service=s)
        self.driver.implicitly_wait(5)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_NewUserBuyItem(self):
        driver = self.driver
        #wait = WebDriverWait(driver, 20)
        #step 1 open site
        driver.get("http://selenium1py.pythonanywhere.com/en-gb/")
        driver.maximize_window()
        #step 2 click button all product
        driver.find_element(By.LINK_TEXT, "All products").click()
        time.sleep(2)
        #step 3 Choose Book and click on button 'Add to Basket'
        driver.find_element(By.XPATH,
            "//body[@id='default']/div[2]/div/div/div/section/div/ol/li/article/div/a/img").click()
        time.sleep(2)
        driver.find_element(By.XPATH, "//button[@value='Add to basket']").click()
        time.sleep(2)
        #step 4 Click on Button 'View Basket'
        driver.find_element(By.LINK_TEXT, "View basket").click()
        time.sleep(2)
        #step 5 Verify that book was added to the basket
        try:
            VerifyBasket = self.driver.find_element(By.ID, "basket_totals").text
            assert "Order total" in VerifyBasket
            print("Assertion is TRUE")

        except Exception as e:
            print("Assertion is FALSE", format(e))
        #step 6 Click 'Proceed to checkout'
        driver.find_element(By.LINK_TEXT, "Proceed to checkout").click()
        time.sleep(2)
        driver.find_element(By.ID, "id_username").click()
        driver.find_element(By.ID, "id_username").clear()
        driver.find_element(By.ID, "id_username").send_keys("testingaja@gmail.com")
        time.sleep(2)
        driver.find_element(By.ID, "id_password").click()
        driver.find_element(By.ID, "id_password").clear()
        driver.find_element(By.ID, "id_password").send_keys("Pass.123")
        time.sleep(2)
        driver.find_element(By.ID, "id_options_1").click()
        time.sleep(2)
        driver.find_element(By.XPATH, "//button[@type='submit']").click()
        time.sleep(2)
        driver.save_screenshot(r'C:\Users\Admin\Documents\PVG\ScenarioTestSCript\Screenshoot\Capture.png')
        print("registration of new user was opened")
    def is_element_present(self, how, what):
        try:
            self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e:
            return False
        return True

    def is_alert_present(self):
        try:
            self.driver.switch_to_alert()
        except NoAlertPresentException as e:
            return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally:
            self.accept_next_alert = True

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)


if __name__ == "__main__":
    unittest.main()
